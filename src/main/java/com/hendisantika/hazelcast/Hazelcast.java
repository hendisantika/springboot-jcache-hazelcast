package com.hendisantika.hazelcast;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.cache.annotation.CacheResult;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-jcache-hazelcast
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 08/02/18
 * Time: 21.26
 * To change this template use File | Settings | File Templates.
 */

@RestController
public class Hazelcast {
    @CacheResult(cacheName = "testCache")
    public String getData(@RequestParam("param") String param) {
        System.out.println("@@@@> Cache miss! Getting new value");

        return param + "_" + System.currentTimeMillis();
    }
}
